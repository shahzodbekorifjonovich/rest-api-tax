package uz.com.restapitax.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserPaymentDto {
    @NotBlank(message = "UserTin may not be blank")
    @Size(max = 9)
    private String tin;
    @NotNull(message = "Summa may not be null")
    private Integer summa;
    @Size(max = 2)
    @NotBlank(message = "TaxCode may not be blank")
    private String code;
    @NotNull(message = "Created_day may not be blank")
    private Date date;
}
