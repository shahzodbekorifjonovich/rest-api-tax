package uz.com.restapitax.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private String address;
    private String tin;
    private String username;
    private Integer summa;
}
