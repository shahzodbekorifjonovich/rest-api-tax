package uz.com.restapitax.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.com.restapitax.entity.TaxPaymentMonitoring;

@Repository
public interface TaxMonitorRepository extends JpaRepository<TaxPaymentMonitoring,Integer> {
}
