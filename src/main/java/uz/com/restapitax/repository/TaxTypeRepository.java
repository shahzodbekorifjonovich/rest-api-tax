package uz.com.restapitax.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.com.restapitax.entity.TaxType;

@Repository
public interface TaxTypeRepository extends JpaRepository<TaxType,Integer> {
    TaxType findByCode(String code);
}
