package uz.com.restapitax.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.com.restapitax.entity.User;

import java.util.Optional;

@Repository
public interface UserRepository  extends JpaRepository<User,Integer> {
    User findByTin(String tin);

}
