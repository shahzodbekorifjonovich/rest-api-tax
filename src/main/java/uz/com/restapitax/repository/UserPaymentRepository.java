package uz.com.restapitax.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.com.restapitax.entity.UserPayment;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserPaymentRepository extends JpaRepository<UserPayment,Integer> {

//    List<UserPayment> findAllByUserId(Integer userId);
//
//    List<UserPayment> findAllById(Integer userId);
}
