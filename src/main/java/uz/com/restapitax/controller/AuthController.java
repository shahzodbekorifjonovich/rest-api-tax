package uz.com.restapitax.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.com.restapitax.dto.UserDto;
import uz.com.restapitax.dto.UserPaymentDto;
import uz.com.restapitax.entity.User;
import uz.com.restapitax.model.ResponseData;
import uz.com.restapitax.service.UserService;

import javax.validation.Valid;
import java.util.List;
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private UserService userService;

    @GetMapping("/user/{id}")
    public UserDto getUser(@PathVariable Integer id) {
        return userService.getUser(id);
    }

    @PostMapping("/payment/user")
    public ResponseData<User> userPayment(@Valid  @RequestBody UserPaymentDto userPaymentDto) {
        return userService.saved(userPaymentDto);
    }

    @GetMapping("/users")
    public List<User> getAllUsers(){
        return userService.getAllUser();
    }
}
