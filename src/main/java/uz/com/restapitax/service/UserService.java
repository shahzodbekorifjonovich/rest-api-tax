package uz.com.restapitax.service;

import uz.com.restapitax.dto.UserDto;
import uz.com.restapitax.dto.UserPaymentDto;
import uz.com.restapitax.entity.User;
import uz.com.restapitax.entity.UserPayment;
import uz.com.restapitax.model.ResponseData;

import java.util.List;

public interface UserService {
    UserDto getUser(Integer id);

    ResponseData<User> saved(UserPaymentDto userPaymentDto);

    List<User> getAllUser();
}
