package uz.com.restapitax.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 *
 * @author shahzod
 * User malumotlari
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Size(max = 9)
    @Column(name = "tin", nullable = false)
    private String tin;
    @Size(max = 250)
    @Column(name = "name", nullable = false)
    private String username;
    @Size(max = 250)
    @Column(name = "address")
    private String address;
    @Column(name = "summa",nullable = false)
    private Integer summa;
    public User(@Size(max = 9) Integer summa) {
        this.summa = summa;
    }
}
